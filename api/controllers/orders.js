const Order = require('../models/order');
const Product = require('../models/product');
const mongoose = require('mongoose');

exports.orders_get_all = (req, res, next) => {
    Order
        .find()
        .select('product quantity _id')
        .populate('product', 'name')
        .exec()
        .then(docs => {
            res.json({
                count: docs.length,
                orders: docs.map(doc => {
                    return {
                        _id: doc._id,
                        product: doc.product,
                        quantity: doc.quantity,
                        request: {
                            type: 'GET',
                            url: 'http://localhost:3000/orders/' + doc._id
                        }
                    }
                })
            });
        })
        .catch(err => {
            res.json({
                error: err
            });
        });
}

exports.orders_create_order = (req, res, next) => {
    Product
        .findById(req.body.productId)
        .then(product => {
            if (!product) {
                return res.json({
                    message: 'Product not found'
                });
            }

            const order = new Order({
                _id: mongoose.Types.ObjectId(),
                quantity: req.body.quantity,
                product: req.body.productId
            });

            return order.save();
        })
        .then(result => {
            console.log(result);
            res.json({
                message: 'Order Saved',
                createdOrder: {
                    _id: result._id,
                    product: result.product,
                    quantity: result.quantity
                },
                request: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders/' + result._id
                }
            });
        })
        .catch(err => {
            console.log(err);
            res.json({ error: err });
        });
}


exports.orders_get_order = (req, res, next) => {
    Order
        .findById(req.params.orderId)
        .select('product quantity _id')
        .populate('product', '_id name price')
        .exec()
        .then(order => {
            if (!order) {
                return res.json({
                    message: 'Order not found'
                });
            }

            res.json({
                order: order,
                resuest: {
                    type: 'GET',
                    url: 'http://localhost:3000/orders'
                }
            });
        })
        .catch(err => {
            res.json({
                error: err
            });
        });
}

exports.orders_delete_order = (req, res, next) => {
    Order
        .deleteOne({ _id: req.params.orderId })
        .exec()
        .then(result => {
            res.json({
                message: 'Order deleted',
                request: {
                    type: 'POST',
                    url: 'http://localhost:3000/orders',
                    body: {
                        productID: 'ID',
                        quantity: 'Number'
                    }
                }
            })
        })
        .catch(err => {
            res.json({
                error: err
            });
        });
}
