const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');

exports.user_signup = (req, res, next) => {
    User
        .find({ email: req.body.email })
        .exec()
        .then(user => {
            if (user.length >= 1) {
                return res.json({
                    message: 'email exists'
                });
            }
            else {
                bcrypt.hash(req.body.password, 10, (err, hash) => {
                    if (err) {
                        return res.json({
                            error: err
                        });
                    }
                    else {
                        const user = new User({
                            _id: new mongoose.Types.ObjectId(),
                            email: req.body.email,
                            password: hash
                        });

                        user
                            .save()
                            .then(result => {
                                console.log(result);
                                res.json({
                                    message: 'User Created'
                                });
                            })
                            .catch(err => {
                                console.log(err);
                                res.json({
                                    error: err
                                })
                            });
                    }
                })
            }
        })
        .catch(err => {
            console.log(err);
            res.json({
                error: err
            });
        });
}

exports.user_login = (req, res, next) => {
    User
        .findOne({ email: req.body.email })
        .exec()
        .then(user => {
            if (!user) {
                return res.json({
                    message: 'failed to login'
                });
            }

            bcrypt.compare(req.body.password, user.password, (err, result) => {
                if (err) {
                    return res.json({
                        message: 'failed to login'
                    });
                }

                if (result) {
                    const token = jwt.sign({
                        email: user.email,
                        userId: user._id
                    },
                        'secret-jwt-key',
                        {
                            expiresIn: '1h'
                        }
                    );

                    return res.json({
                        message: 'successful login',
                        token: token
                    });
                }

                return res.json({
                    message: 'failed to login'
                });
            });
        })
        .catch(err => {
            console.log(err);

            res.json({
                error: err
            });
        });
}

exports.user_delete_user = (req, res, next) => {
    User
        .deleteOne({
            _id: req.params.userId
        })
        .exec()
        .then(user => {
            console.log('user deleted');

            res.json({
                message: 'user deleted'
            });
        })
        .catch(err => {
            console.log(err);

            res.json({
                error: err
            });
        });
}

